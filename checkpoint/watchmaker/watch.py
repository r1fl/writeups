#!/bin/python

import os, time
from subprocess import Popen, PIPE

import sys

def main():
	proc = Popen('patience.exe', stdin=PIPE, stdout=PIPE)
	
	for i in xrange(2): print(proc.stdout.readline()),
	
	key = '400487368126745'
	key = ''
	prev_time = 0
	
	num = 0
	while True:
		time = run(proc, key + str(num))
		if  time > prev_time:
			prev_time = time
			key = key + str(num)
			
			num = 0
			print('0.{}: {}'.format(prev_time, key))
			
		else: num +=1
			
	proc.kill()
	
	
def run(proc, key):
	t1 = time.time()
	proc.stdin.write(key+'\n'),
	out = proc.stdout.readline()
	t2 = time.time()
	
	if out.find('Wrong') != -1:
		return int((t2-t1) / 0.1)
		
	print(out)
	sys.exit()
	
if __name__ == '__main__':
	main()
	