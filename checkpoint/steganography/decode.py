#!/bin/python3

import math
import argparse
import sys, os, struct
import ref_enc as gif

def main(args):
	size = os.path.getsize(args.file)
	with open(args.file, 'rb') as file:
		global_colors, bgcolor_index, size_count, height, width = gif.format(file)
		hdr = file.tell()

		shf_key = get_shuffled(file, hdr)
		print(shf_key)
		# print(len(global_colors))

		blocks = []

		pos = file.tell()
		while pos < size:
			a = file.read(4)

			if a.hex() == '21f90405':
				blocks.append(pos)
				pos += 4

			else:
				pos += 1
				file.seek(pos, 0)

		for blk in blocks:
			print(read_gc_block(file, blk))

		key = ''
		for p in blocks:
			block = read_gc_block(file, p)
			# print(block)
			delay, tidx, x, xx, xxx, xxxx = block

			upper = (tidx % 2)
			indx = get_indx(x, xx, xxx, xxxx, width, height, len(shf_key))

			if indx != None:
				if not upper:
					key += shf_key[indx].lower()
				else:
					key += shf_key[indx]
			else:
				key += 'x'

		print(key)

# x, xx, xxx, xxxx = (_0, _1, _3, _4)
def get_indx(x, xx, xxx, xxxx, ww, hh, shf_len):
	_0, _1, _3, _4 = x, xx, xxx, xxxx

	# indx < 8
	if _0 == 0:
		if _1 == 1:
			if _3 >= 4 and _3 <= ww-1:
				a = _4 >> 2
				if a % 2 and a < 8:
					return a

	if _0 == 1:
		if _3 >= 4 and 3 <= ww/2:
			if _4 >= 4 and _4 <= hh/3:
				a = _1 >> 1
				if not a % 2 and a < 8:
					return a

	if _0 == _1 == 0:
		if _4 >= 1:
			for a in range(8, shf_len):
				if _4 <= math.sqrt(a) + 1:
					if a % _4 == 0:
						if a/_4 == _3:
							return a

	print('nope')
	return None

	raise Exception('invalid input')


def read_gc_block(file, pos):
	restore_pos = file.tell()

	file.seek(pos, 0)
	assert file.read(4).hex() == '21f90405', 'invalid block'

	delay, tidx = struct.unpack('HB', file.read(3))

	assert file.read(1).hex() == '00', 'block terminator not found'
	assert file.read(1).hex() == '2c', ''

	x, xx, xxx, xxxx = struct.unpack('HHHH', file.read(8))
	assert file.read(1).hex() == '00', 'block terminator not found'

	file.seek(restore_pos, 0)
	return delay, tidx, x, xx, xxx, xxxx # (d, xxx, xxxx, x, xx, idx)


def get_shuffled(file, hdr):
	file.seek(hdr+2)
	size = struct.unpack('B', file.read(1))[0]
	shuffled_flag = file.read(size).decode('ascii')[len('RDBNB')::]

	return shuffled_flag

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('file', help='GIF File to be decoded.')
	
	main(parser.parse_args())