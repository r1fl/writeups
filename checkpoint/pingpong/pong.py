#!/bin/python3

import socket
import argparse, sys

def main(args):
	# nc 35.157.111.68 10075
	
	s = socket.socket()
	s.connect(('35.157.111.68', 10126))

	print(s.recv(1024))

	a = s.recv(1024)
	while True:
		msg = str(a)[str(a).find(':')+1:-3].strip() + '\n'
		s.send(msg.encode('ascii'))
		a = s.recv(1024)
		args.log.write(a.decode('ascii'))
		print(a.decode('ascii'))

		
	# print(str(i) + ' ' + repr(data))
		# i += 1

	s.close()
	args.log.close()



if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('--log', default=sys.stdout, type=argparse.FileType('w'))
	main(parser.parse_args())

# str(a)[str(a).find(':')+1:-3].strip().encode('ascii')
