# Ping Pong
###### (Networking, 25 pts)

Instructions:
```
I bet you're not fast enough to defeat me.
I'm at: nc 35.157.111.68 10075
```

We are given an ip and a port to netcat into, let's try that:
![netcat](https://i.imgur.com/5kn2g0T.png)

huh, typing it by hand looks tedious - let's try and automate that.

```py
s = socket.socket()
s.connect(('35.157.111.68', 10126))

print(s.recv(1024))

a = s.recv(1024)
while True:
	msg = str(a)[str(a).find(':')+1:-3].strip() + '\n'
	s.send(msg.encode('ascii'))
	a = s.recv(1024)
	print(a.decode('ascii'))


s.close()
```

that way, we will recieve the string from the server, recieve the number, encode it and resend it.
in the final script some logging stuff was added but the idea remains the same.

running it produces a thousand responses and the thousandth is the flag.
```
flag{SEeMs_LIkE_YOu_STilL_Fa$t}
```
