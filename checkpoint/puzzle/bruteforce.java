import java.util.*;
import java.io.IOException;

class bruteforce {
	private static int key_length;
	private static int[] key;
	private static int[] ans;

	private static int cnt = 0;

	public static void main(String[] args) throws Exception
	{
		try {
			key_length = Integer.parseInt(args[0]);
		} catch(Exception e) {
			throw new IOException("bad arguments.");
		}
		
		key = new int[key_length];
		ans = new int[key_length];

		Random rand = new Random();
		for(int i = 0; i < key_length; i++) {
			key[i] = 0;
			ans[i] = rand.nextInt(10);
			// ans[i] = 9;
		}		

		System.out.println("key: " + Arrays.toString(key));
		System.out.println("ans: " + Arrays.toString(ans));
		
		// System.out.println(Arrays.toString(to_array(10)));
		
		if(brute())
			System.out.println("key: " + Arrays.toString(key));

		// if(brute_r(0))
		// 	System.out.println("key: " + Arrays.toString(key));
	}

	public static boolean brute() {
		int lim = (int) Math.pow(10, key_length);

		for(int i = 0; i < lim; i++) {
			key = to_array(i);
			System.out.println(Arrays.toString(key));
			if(Arrays.equals(key, ans)) {
				return true;
			}
		}

		return false;
	}

	public static int[] to_array(int num) {
		String str = Integer.toString(num);
		int[] ret = new int[key_length];

		for(int i = 0; i < str.length(); i++) {
			ret[key_length-i-1] = Character.getNumericValue(str.charAt(str.length()-i-1));
		}

		return ret;
	}

	public static boolean brute_r(int pos) {
		for(int i = 0; i < 10; i++) {
			key[pos] = i;
			// System.out.println(Arrays.toString(key));

			if(Arrays.equals(key, ans))
				return true;

			if(pos < key_length-1) {
				if(brute_r(pos+1))
					return true;
			}
		}

		key[pos] = 0;
		return false;
	}
}