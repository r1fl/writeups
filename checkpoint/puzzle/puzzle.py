#!/bin/python3

import argparse
import sys, os
import numpy, math
import time

sqrt = lambda n: (False, int(math.sqrt(n)))[int(math.sqrt(n)) == math.sqrt(n)]  # natural sqrt of n, false if rational sqrt

clear = lambda: sys.stdout.write(os.popen('clear').read()) # linux clear

stripm = lambda m: [i[1:-1] for i in m[1:-1]] # strip one layer from the matrix
printm = lambda m: print(numpy.array2string(numpy.matrix(m), max_line_width=numpy.inf), flush=True) # print matrix
printsm = lambda m: printm(stripm(m)) # print stripped matrix


def main(args):

	with open(args.file, 'r') as file:
		pcs = file.read().strip("\'").split('; ')

	pcs = [piece(p) for p in pcs] # list of pieces

	size = [sqrt(len(pcs))]*2 # (height, width)
	pzl = sort(pcs, *size)
	printm(pzl)
	print(' ')


	flag = ''
	for i in pzl:
		for j in i:
			flag += '{},{}; '.format(j.id, j.rotates())

	print(flag[:-2])


def sort(pcs, hh, ww):

	# hh = ww = 31

	pzl = [[piece()] * (ww+2)] + \
			[[piece()] + [None] * ww + [piece()] for i in range(hh)] + \
			[[piece()] * (ww+2)]

	hh = ww = ww+2 # puzzle is padded with border made out of [0,0,0,0] pcs

	target_indxs = [] # indexes of the matrix in spiral e.g. for 3x3 [(1,0), (1,1), (1,2), (2,2)...]
	for i in range(int((hh-2)/2)):
		# target_indxs += [top] + [right] + [bottom] + [left]
		target_indxs += [(1+i, x) for x in range(1+i, ww-1-i)] + \
						[(y, ww-2-i) for y in range(2+i, hh-1-i)] + \
						[(hh-2-i, x) for x in range(ww-3-i, 1+i, -1)] + \
						[(y, 1+i) for y in range(hh-2-i, 1+i, -1)]

	if hh%2 != 0: target_indxs.append((math.ceil(hh/2)-1, math.ceil(hh/2)-1)) # center piece if uneven (because (hh-2)/2 is being floored)

	if sort_pieces(pzl, pcs, target_indxs):
		# printsm(pzl)
		print('True')
		return stripm(pzl)

	return False


def sort_pieces(pzl, pcs, indxs, i=0):
	if i == len(indxs):
		return True

	(row, col) = indxs[i]
	_fit_pcs = filter_pcs(pzl, row, col, pcs)

	for piece in _fit_pcs:

		pzl[row][col] = piece
		if adjust(pzl, row, col):
			# printsm(pzl)
			# time.sleep(0.3)
			# clear()

			_pcs = [p for p in pcs if p.id != piece.id]
			if sort_pieces(pzl, _pcs, indxs, i+1):
				return True

		pzl[row][col] = None

	return False


def filter_pcs(pzl, row, col, pcs):
	ret = []

	for piece in pcs:
		pzl[row][col] = piece

		if adjust(pzl, row, col):
			ret.append(piece)

	pzl[row][col] = None
	return ret


def adjust(pzl, row, col):
	return pzl[row][col].rotate_to([
		None if pzl[row-1][col] is None else pzl[row-1][col].piece[2], # top-face
		None if pzl[row][col+1] is None else pzl[row][col+1].piece[3], # right-face
		None if pzl[row+1][col] is None else pzl[row+1][col].piece[0], # bottom-face
		None if pzl[row][col-1] is None else pzl[row][col-1].piece[1], # left-face
	])


class piece:
	def __init__(self, pstr=None):
		if pstr:
			pstr = (pstr[:pstr.find(',')], pstr[pstr.find(',')+1:])

			self.id = int(pstr[0])
			self.rot = 0
			
			self.original = [int(i) for i in pstr[1][1:-1].split(', ')]
			self.piece = self.original.copy()


		else:
			self.id = None

			self.original = [0] * 4
			self.piece = self.original.copy()

	__str__ = lambda self: str(self.piece)

	__repr__ = lambda self: self.__str__()

	def rotates(self):
		return self.rot % 4

	def rotate(self):
		self.rot += 1

		tmp = self.piece
		self.piece = [tmp[3], tmp[0], tmp[1], tmp[2]]

	def rotate_to(self, tar):

		for rot in range(4):

			for i, j in enumerate(tar):
				done = True
				if None != j != self.piece[i]:
					self.rotate()
					done = False
					break

			if done:
				return True

		return False


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Solves puzzle, pass your puzzle file as an argument. Syntax: id,[top,right,buttom,left];')
	parser.add_argument('file', help='File containing the puzzle string.')
	main(parser.parse_args())
