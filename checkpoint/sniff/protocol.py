#!/bin/python3

import socket

def main():
	# get file contents and key

	global INDEX
	INDEX = 0

	IP = '35.157.111.68'
	PORT = 20026

	s = socket.socket()
	s.connect((IP, PORT))

	print(s.recv(1024))

	send(s, 'HELLO')
	print(s.recv(1024))

	send(s, 'XOR')
	key = s.recv(1024)
	print(key)

	send(s, '/usr/top_secr3T.txt')
	content = s.recv(1024)
	print(content)

	s.close()

def send(s, msg):
	global INDEX
	INDEX += 1

	msg = '{} {} {}\n'.format(INDEX, len(msg), msg)
	s.send(msg.encode('ascii'))

if __name__ == '__main__':
	main()