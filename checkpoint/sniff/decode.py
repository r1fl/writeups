#!/bin/python3

# decrypt

key = "94B8"
msg = "0xf2d40xf5df0xefe10xa4cd0xcb9b0xd5ee0xf1e70xf3d70xe0e70xfdcc0xb5c5".split('0x')
msg.remove('')

print(msg)

for i in msg:
	byte = int('0x'+i[:2], 0)
	xor = int('0x'+key[:2], 0)
	print(chr(byte^xor), end='')

	byte = int('0x'+i[2:], 0)
	xor = int('0x'+key[2:], 0)
	print(chr(byte^xor), end='')

print('\n')