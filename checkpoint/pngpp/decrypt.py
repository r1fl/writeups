#!/bin/python3

import argparse

HEADERS = '137 80 78 71 13 10 26 10'.split()
key_length = 4

def main(args):
	with open(args.file, 'rb') as f:
		img = f.read()

	key = get_initial_key(img, key_length)
	flag = b''

	for i in range(0, len(img), 4):
		s1 = img[i:i+4]

		for j in range(key_length):
			flag += (s1[j] ^ ord(key[j])).to_bytes(1, 'big')

		key = key_transform(key)

	with open('flag.png', 'wb+') as f:
		f.write(flag)

def get_initial_key(file, key_length):
	ret = ''

	for i in range(0, key_length):
		ret += chr(file[i]^int(HEADERS[i]))

	return ret

def key_transform(key):
	ret = ''
	for i in key:
		if ord(i) == 255:
			ret += chr(0)
			continue

		ret += chr(ord(i)+1)

	return ret


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('file', help='file to decrypt')
	main(parser.parse_args())