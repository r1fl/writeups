# PNG++ 
###### (Logic, 30 pts)

Instructions:
```
This image was encrypted using a custom cipher.
We managed to get most of its code here
Unfortunately, while moving things around, someone spilled coffee all over key_transformator.py.
Can you help us decrypt the image? 
```

So we're given an encrypt script with a key that is changing for what seems to be every four bytes.

```py
import key_transformator
import random
import string

key_length = 4

def generate_initial_key():
    return ''.join(random.choice(string.ascii_uppercase) for _ in range(4))


def xor(s1, s2):
    res = [chr(0)]*key_length
    for i in range(len(res)):
        q = ord(s1[i])
        d = ord(s2[i])
        k = q ^ d
        res[i] = chr(k)
    res = ''.join(res)
    return res


def add_padding(img):
    l = key_length - len(img)%key_length
    img += chr(l)*l
    return img


with open('flag.png', 'rb') as f:
    img = f.read()

img = add_padding(img)
key = generate_initial_key()

enc_data = ''
for i in range(0, len(img), key_length):
    enc = xor(img[i:i+key_length], key)
    key = key_transformator.transform(key)
    enc_data += enc

with open('encrypted.png', 'wb') as f:
    f.write(enc_data)
```

Looking at the code we can note that the transform function is missing, thus, to decrypt the image we shall find the initial key and the how it changes.

We can also note that the encryptions is done via a xor encryption, which is infamous for the lack of effort needed to find the key when given the encrypted and decrypted data (xor(enc, dec) = key).

As we are told, the decrypted file is a png file, and reading the png specification tells us there are 8 constant bytes (magic number) that imply the image is a png image. Great! We could probably use that to get the first two keys!

```py
HEADERS = '137 80 78 71 13 10 26 10'.split()
key_length = 4

def get_initial_key(file, key_length):
	ret = ''

	for i in range(0, key_length):
		ret += chr(file[i]^int(HEADERS[i]))

	return ret
```

so the initial key is "NLET".
doing the same thing for the next chunk reveal that the next key is "OMFU".

'O' is ``chr(ord('N')+1)``,  
'M' is ``chr(ord('L')+1)``,  
'F' is ``chr(ord('E')+1)``,  
'U' is ``chr(ord('T')+1)``  

the challange name now makes perfect sense!
let's code it!

```py
def key_transform(key):
	ret = ''
	for i in key:
		if ord(i) == 255:
			ret += chr(0)
			continue

		ret += chr(ord(i)+1)

	print(ret)
	return ret
```

decrypting the image is now just a matter of putting it all together.

```py
key = get_initial_key(img, key_length)
flag = b''

for i in range(0, len(img), 4):
	s1 = img[i:i+4]

	for j in range(key_length):
		flag += (s1[j] ^ ord(key[j])).to_bytes(1, 'big')

	key = key_transform(key)

with open('flag.png', 'wb+') as f:
	f.write(flag)
```

![flag](https://i.imgur.com/pbexoSI.png)

Nice!
