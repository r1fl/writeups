# 0120343536
###### (Login, 60 pts)

Instructions:

```
flag{MENQ_NQ_RZM_%_APCBMZ_F%NIDPV}
Not so fast...
They say the only place where flags come before work is the dictionary, 
ours
is no different
Note: flag 
letters are all capital
```

We are given a dictionary of over 30,000 words, and a format in which the flag is constructed.
First, lets make a set of dictionary words to avoid any duplicates.

```py
with open("dictionary.txt") as f:
	data = f.read()

data = data.split('\r\n')
data = set(data)
```

The flag format also tells us there are six words in lengths of 4, 2, 3, 1, 6 and 7 respectively.
We can get all possible words for each parts of the flag like so,
```py
word_length_1 = filter(lambda x: len(x) == 1, data)
word_length_2 = filter(lambda x: len(x) == 2, data)
word_length_3 = filter(lambda x: len(x) == 3, data)
word_length_4 = filter(lambda x: len(x) == 4, data)
word_length_6 = filter(lambda x: len(x) == 6, data)
word_length_7 = filter(lambda x: len(x) == 7, data)
```

according to the flag format, some words share the same characters - we could leverage that to find the words in which all characters match.

now bare with me, it's gonna get ugly

```py
for w1 in word_length_1:
        for w2 in word_length_2:
            if w2[0] != w2[1]:
                for w4 in word_length_4:
                    if w4[2] == w2[0] and w4[3] == w2[1] :
                        for w3 in word_length_3:
                            if w3[2] == w4[0]:
                                for w6 in word_length_6:
                                    if w6[4] == w4[0] == w3[2] and w6[5] == w3[1]:
                                        for w7 in word_length_7:
                                            if w7[1] == w1[0] and w7[2] == w2[0] and w7[5] == w6[1] and d(w7) and d(w6) and d(w4) and d(w3) and d(w2):
                                                print w4, w2, w3, w1, w6, w7
```

like so we will find the combination of words in which all characters match,

e.g. we look at the following line
```py
if w4[2] == w2[0] and w4[3] == w2[1] :
```

looking at the flag format it becomes pretty obvious
flag{ME**N**~~Q~~\_**N**~~Q~~\_RZM\_\%_APCBMZ\_F\%NIDPV}

running the script will produce the following output
```
THIS IS NOT A CRYPTO FAILURE
```

now all thats left is wrapping it in the flag wrapper and submitting it.
```
flag{THIS IS NOT A CRYPTO FAILURE}
```
fantastic!

