#!/bin/python2

def main():
    for_itamar()

def for_itamar():
    def d(b):
        """
        Check if all the chars in the sting are different
        :param b: 
        :return: 
        """
        return bool(all([b[i] not in b[:i] + b[i + 1:] for i in range(len(b))]))
    flag = "flag{MENQ_NQ_RZM_%_APCBMZ_F%NIDPV}"
    
    with open("dictionary.txt", 'rb') as f:
        data = f.read()
    data = data.split('\r\n')
    data = set(data)


    word_length_1 = filter(lambda x: len(x) == 1, data)
    word_length_2 = filter(lambda x: len(x) == 2, data)
    word_length_3 = filter(lambda x: len(x) == 3, data)
    word_length_4 = filter(lambda x: len(x) == 4, data)
   # word_length_5 = filter(lambda x: len(x) == 5, data)
    word_length_6 = filter(lambda x: len(x) == 6, data)
    word_length_7 = filter(lambda x: len(x) == 7, data)

    for w1 in word_length_1:
        for w2 in word_length_2:
            if w2[0] != w2[1]:
                for w4 in word_length_4:
                    if w4[2] == w2[0] and w4[3] == w2[1] :
                        for w3 in word_length_3:
                            if w3[2] == w4[0]:
                                for w6 in word_length_6:
                                    if w6[4] == w4[0] == w3[2] and w6[5] == w3[1]:
                                        for w7 in word_length_7:
                                            if w7[1] == w1[0] and w7[2] == w2[0] and w7[5] == w6[1] and d(w7) and d(w6) and d(w4) and d(w3) and d(w2):
                                                print w4, w2, w3, w1, w6, w7

if __name__ == '__main__':
    main()
