#!/bin/python3

import sys
flag = 'MENQ_NQ_RZM_%_APCBMZ_F%NIDPV'.split('_')

def main():
	with open('dictionary.txt', 'r') as f:
		tmp = f.read()
	dictionary = tmp.split('\n')

	# THIS_IS_NOT_A_CRYPTO

	# % => A
	# M => T
	# E => H
	# N => I
	# Q => S
	# R => N
	# Z => O

	# A => C
	# P => R
	# C => Y
	# B => P

	for i in dictionary:
		if len(i) == 7:
			print(i)

	sys.exit()

	words = {flag_word:[] for flag_word in flag} # ['FOO', 'BAR'...]
	for flag_word in flag:
		tmp = len(flag_word)

		for word in dictionary:
			if len(word) == tmp:
				words[flag_word].append(word)

	letters = {flag_word:{flag_word_letter:[] for flag_word_letter in flag_word} for flag_word in flag} # {'FOO': {'F': [], 'O':[]...}}
	for flag_word in words:
		tmp = len(flag_word)

		for word in words[flag_word]:
			for carriage_pos in range(tmp):
				if not word[carriage_pos] in letters[flag_word][flag_word[carriage_pos]]:
					letters[flag_word][flag_word[carriage_pos]].append(word[carriage_pos])

	# print(is_valid(letters, '%', 'A'))
	tmp = {flag_word:{flag_word_letter:[] for flag_word_letter in flag_word} for flag_word in flag} # {'FOO': {'F': [], 'O':[]...}}
	for flag_word, flag_word_letters in letters.items():
		for flag_word_letter in flag_word_letters:
			for flag_word_letter_trans in flag_word_letters[flag_word_letter]:
				if is_valid(letters, flag_word_letter, flag_word_letter_trans):
					if not flag_word_letter_trans in tmp[flag_word][flag_word_letter]:
						tmp[flag_word][flag_word_letter].append(flag_word_letter_trans)
	
	# print(tmp['RZM'])
	for i, j in tmp['RZM'].items():
		print(j)

	print(words)
	for i in tmp['RZM']['R']:
		for j in tmp['RZM']['Z']:
			for k in tmp['RZM']['M']:
				# if (i+j+k) in words:
				continue


def is_valid(letters, letter, trans):
	for flag_word, flag_word_letters in letters.items():
		if letter in flag_word_letters:
			if not trans in flag_word_letters[letter]:
				return False
	return True

if __name__ == '__main__':
	main()