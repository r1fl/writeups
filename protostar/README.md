## TODO

- stack6 rop

## CHALLS

### stack

- [x] stack0
- [x] stack1
- [x] stack2
- [x] stack3
- [x] stack4
- [x] stack5
- [x] stack6
- [ ] stack7

### format

- [x] format0
- [ ] format1
- [ ] format2
- [ ] format3
- [ ] format4

### heap

- [x] heap0
- [x] heap1
- [x] heap2
- [ ] heap3
