#!/usr/bin/env python

import argparse, csv
#import unittest 


from difflib import SequenceMatcher
from itertools import permutations

PREFIXES = ['ms', 'ms.', 'mrs', 'mrs.']
TYPO_THRESHOLD = 0.7

def main(args):

    global detector
    detector = Sameonator(args.names)

    assert countUniqueNames('Deborah','Egli','Deborah','Egli','Deborah Egli') == 1
    assert countUniqueNames('Deborah','Egli','Debbie','Egli','Debbie Egli') == 1
    assert countUniqueNames('Deborah','Egni','Deborah','Egli','Deborah Egli') == 1
    assert countUniqueNames('Deborah S','Egli','Deborah','Egli','Egli Deborah') == 1
    assert countUniqueNames('Michele','Egli','Deborah','Egli','Michele Egli') == 2

    print("[5/5] all good")

def countUniqueNames(billFirstName, billLastName, shipFirstName, shipLastName, billNameOnCard):
    billNameOnCard = billNameOnCard.split()

    # leave only first, median and last name (might want to filter weird suffixes too)
    billNameOnCard = list(filter(lambda s: s not in PREFIXES, billNameOnCard))

    billFirstNameOnCard = billNameOnCard[0]
    billLastNameOnCard = billNameOnCard[len(billNameOnCard)-1]

    # ¯\_(ツ)_/¯

    if sameperson(billFirstName, billLastName, shipFirstName, shipLastName): # bill = ship
        if sameperson(billFirstName, billLastName, billFirstNameOnCard, billLastNameOnCard): # bill = on card
            return 1
        if sameperson(shipFirstName, shipLastName, billFirstNameOnCard, billLastNameOnCard): # ship = on card
            return 1

        return 2

    if sameperson(billFirstName, billLastName, billFirstNameOnCard, billLastNameOnCard): # bill = on card
        return 2

    if sameperson(shipFirstName, shipLastName, billFirstNameOnCard, billLastNameOnCard): # ship = on card
        return 2

    return 3


def sameperson(firstname1, lastname1, firstname2, lastname2):
    assert 'detector' in globals()
    return detector.isalike(firstname1, firstname2) and detector.isalike(lastname1, lastname2)

def sameperson(*names):
    """
    returns true if names can be paired
    in a way that a pairs are a like.
    """

    assert 'detector' in globals()
    names = list(names)

    if len(names) == 0:
        return True
    
    for n1, n2 in permutations(names, r=2):
        if detector.isalike(n1, n2):
            names.remove(n1)
            names.remove(n2)

            return sameperson(*names)

    return False

class Sameonator:

    def __init__(self, fname):
        self.names = []

        with open(fname, 'r', newline='') as f:
            rdr = csv.reader(f)
            self.names = list(rdr)

    def isalike(self, n1, n2):
        """
        is nick *or* similar
        """
        n1, n2 = map(str.lower, (n1, n2))

        if self.similarity(n1, n2) >= TYPO_THRESHOLD:
            return True

        if self.isnick(n1, n2):
            return True

        return False

    def isnick(self, n1, n2):
        nicks = []

        for i in self.names:
            if (n1 in i) or (n2 in i):
                nicks.append(i)

        for i in nicks:
            if (n1 in i) and (n2 in i):
                return True

        return False

    def similarity(self, n1, n2):
        return SequenceMatcher(a=n1, b=n2).ratio()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('names', help='names csv file')

    main(parser.parse_args())

