#!/bin/env python3

import requests, urllib
from bs4 import BeautifulSoup
import base64, sys

BASE = 'http://fun.ritsec.club:8007/'
CRAWLED = []

def main():
    crawl(BASE)

def crawl(url):
    global CRAWLED

    print(f'[*] fetching {url}')

    r = requests.get(url)
    s = BeautifulSoup(r.text, 'html.parser')

    if len(s.find_all('iframe')) > 0:
        print(f'[!] iframe at {url}')

        if 'NOTE' in str(s):
            print()
            print(base64.decodebytes(s.p.text.encode()).decode())
            sys.exit()

    for link in s.find_all('a'):
        url = urllib.parse.urljoin(BASE, link.get('href'))

        if url not in CRAWLED:
            CRAWLED.append(url)
            crawl(url)

    return

if __name__ == '__main__':
    main()
