# captcha solver
###### this challenge was apart of owasp-il18

simple captcha solver made for owasp ctf challenge

### dependencies:

- [tesseract-ocr](https://github.com/tesseract-ocr/tesseract) - google's well trained ocr, has to be in $PATH
- [pytesseract](https://github.com/madmaze/pytesseract) - python front end for tesseract

### notes:

- resampling is made with color extraction.
- ocr is using google's tesseract.
- the ai has to be trained on the fonts of the captcha for best results.
- useful [reading material](https://boyter.org/decoding-captchas/)

resampled example:  
![Imgur](https://i.imgur.com/45esWmq.png)

### tests:

- [test](https://i.imgur.com/hz3JDVt.png)
- [test1](https://i.imgur.com/xK9NZUv.png)

### todo:

* [ ] stroke resampled text to improve tesseract detection.
* [ ] generate training data for php gd default font for better results.
