#!/bin/env python3

import argparse

def main(args):

    print("[*] reading file")
    bits = args.file.read().split('.')
    bits = list(filter(lambda s: '-' in s, bits))
    bits = [len(b) for b in bits] 

    alen = indexof(max(bits, key=indexof)) + 1
    print("[?] string length is {}".format(alen))

    print("[*] decoding message")

    flag = ""
    for indx in range(alen):
        char = 0

        for b in bits:
            pos = indexof(b, alen*-1) * -1

            if pos == indx:
                bitpos = (b - (alen - 1 - pos)*8) - 1
                char += 2**bitpos

        flag += chr(char)

    print(flag)


def indexof(a, delta=0):

    for j in range(8):
        len_ = (a - 1 - j) / 8

        if len_ != int(len_): # non-integer
            continue

        return (int(len_) + 1 + delta) # alen - 1 - i


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="square ctf dot-n-dash solution")
    parser.add_argument('file', type=argparse.FileType('r'), help="instructions file to decode")
    main(parser.parse_args())
