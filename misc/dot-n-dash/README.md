# C1: dot-n-dash
###### (Programming)
###### this challange was apart of squarectf 18

Instructions:
```
 The instructions to disable C1 were considered restricted. As a result, they were stored only in encoded form.

The code to decode the instructions was regrettably lost due to cosmic radiation. However, the encoder survived.

Can you still decode the instructions to disable C1? 
```

usage:
``./decode.py instructions.txt``
